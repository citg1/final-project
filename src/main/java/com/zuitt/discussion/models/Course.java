package com.zuitt.discussion.models;

import javax.persistence.*;

@Entity
@Table(name = "courses")
public class Course {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;
    @Column
    private String description;
    @Column
    private Double price;

    @ManyToOne
//    sets the relationship to this property and user_id column in the database to the primary key of the user model.
    @JoinColumn(name="user_id", nullable = false)
    private User user;


    public Course(){}

    public Course(String name, String description, Double price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
    public User getUser(){
        return user;
    }

    public void setUser(User user){
        this.user = user;
    }

}
